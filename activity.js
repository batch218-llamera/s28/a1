// Insert one
db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});

// Insert many
db.rooms.insertMany([
	{
	name: "double",
	accommodates: 3,
	price: 2000,
	description: "A room fit for a small family going on vacation",
	rooms_available: 5,
	isAvailable: false
	},
	{
	name: "queen",
	accommodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false	
	}
]);

// Find a specific document
db.rooms.find({name: "double"});

// Update queen rooms_available
db.users.updateOne(
	{ name: "queen"},
	{
		$set: {
			rooms_available: 0
		}
	} 
);

// Delete rooms_availabe with 0
db.users.deleteMany({
	rooms_available: 0,
});


db.rooms.find();